import React, { Component } from 'react';
import {
  Col, Button, DropdownButton, MenuItem,
  Alert,
  Modal, FormControl,
} from 'react-bootstrap';
import { browserHistory, Link, } from 'react-router';
import firebase from 'firebase';
import slug from 'slug';
import ReactQuill from "react-quill";

/** COMPONENTS CUSTOM **/
import { APP_CONFIG } from './../../../boot.config';
import { TOOLS } from './../../../util/tools.global';
import { MasterForm } from './../../Global/Form/MasterForm';
// import { Input } from '../../Form/Input';
export class Category extends Component{
  constructor(props){
    super(props);
    this.state = {
      appID: document.querySelector('[data-js="root"]').dataset.appid || null,

      isLoading : true,
      showModal : false,
      confirmDelete : false,
      action : this.props.action,
      pages: [],
      categories : [],

      uid : this.props.currentUser.uid,
      categoryID : null,
      title : null,
      slug : null,
      description : '',
      parent : 'Categoria Master',
      status : true,
      createAt: TOOLS.getDate(),
      updateAt: TOOLS.getDate(),
      haveposts : false,

      status_response : null,
      message_response : null
    }

    this.checkStatusResponse = this.checkStatusResponse.bind(this);
    this.checkStates = this.checkStates.bind(this);
    this.getCategory = this.getCategory.bind(this);
    this.resetCurrentState = this.resetCurrentState.bind(this);
    this.renderModal = this.renderModal.bind(this);
    this.requiredInput = TOOLS.requiredInput.bind(this);

    /*** FUNCTIONS AUXILIARES DO FORM ***/
    this.onLoadForm = this.onLoadForm.bind(this);
    this.onSubmitForm = this.onSubmitForm.bind(this);
    this.onUpdateForm = this.onUpdateForm.bind(this);
    this.onChangeForm = this.onChangeForm.bind(this);
    this._onChangeTitle = this._onChangeTitle.bind(this);
    this.onChangeDescription = this.onChangeDescription.bind(this);
    this.renderParent = this.renderParent.bind(this);
    this.onSelectCategory = this.onSelectCategory.bind(this);
    this.statusInfo = this.statusInfo.bind(this);

    /*** RENDER FUNCTIONS - ACTIONS ***/
    this.actionNull = this.actionNull.bind(this);
    this.actionNew = this.actionNew.bind(this);
    this.actionDelete = this.actionDelete.bind(this);
    this.renderAction = this.renderAction.bind(this);

    if(this.props.categoryID){
      this.getCategory(this.props.categoryID);
    }
  }

  componentWillMount(){
    // this.getCategory();
    if(this.props.action === 'edit' && this.props.categoryID){
      this.setState({ categoryID : this.props.categoryID });
    }
  }

  componentDidMount(){
    console.log('User id -> ',this.state.uid);
    let _self=this;
    _self.allPages = firebase.database().ref('apps/'+ this.state.appID +'/pages/');
    _self.allCategories = firebase.database().ref('apps/'+ this.state.appID +'/categories/');
    _self.pages = this.state.pages;
    _self.categories = this.state.categories;

    /** child_added **/
    _self.allCategories.on('child_added', (data) => {
      _self.categories.push(data.val());
      if(data){
        if(_self.categories.length > 0 )
        _self.setState({ categories : _self.categories, isLoading : false });
      }
    });
    /** child_added **/

    /** child_changed **/
    _self.allCategories.on('child_changed', (data) => {
      if(data){
        let changed = data.val();
        if(_self.categories.length > 0 ){
          console.log('child_changed allCategories: ', {changed : changed});
          // eslint-disable-next-line
          _self.categories.map((category) => {
            if(category.categoryID === changed.categoryID){
              category.uid = this.state.uid;
              category.title = changed.title;
              category.description = changed.description;
              category.parent = changed.parent;
              category.status = changed.status;
            }
          });
          _self.setState({ categories : _self.categories, isLoading : false });
        }
      }
    });
    /** child_changed **/

    /** child_moved **/
    _self.allCategories.on('child_moved', (data) => {
      _self.categories.push(data.val());
      if(data){
        if(_self.categories.length > 0 )
        _self.setState({ categories : _self.categories, isLoading : false });
      }
      console.log('child_moved allCategories: ', {qtd: _self.allCategories.length, all: _self.allCategories});
    });
    /** child_moved **/

    /** child_removed **/
    _self.allCategories.on('child_removed', (data) => {
      if(data){
        if(_self.categories.length > 0 ){
          let categoryRemoved = data.val();
          console.log('res child_removed -> ',categoryRemoved);
          // eslint-disable-next-line
          _self.categories.map((category, key) => {
            // eslint-disable-next-line
            if(category.categoryID === categoryRemoved.categoryID){
              console.log('Key -> ',key);
              TOOLS.removeArrayItem(_self.categories,key,1);
              console.log('Category removeed ',categoryRemoved.categoryID);
              _self.setState({ categories : _self.categories, isLoading : false });
            }else{
              console.log('Key -> ',key);
              console.log('Category categoryID ',category.categoryID);
              console.log('Category removeed ',categoryRemoved.categoryID);
            }
          });
        }
      }
    });
    /** child_removed **/
  };//componentDidMount();

  componentWillUnmount(){
    console.clear();
  };//componentWillUnmount();

  componentWillReceiveProps(nextProps){
    console.log('componentWillReceiveProps ',nextProps);

    if(nextProps.action === 'new'){
      return this.resetCurrentState();
    }else if(nextProps.categoryID || nextProps.action === 'edit'){
      console.log('getCategory to ',nextProps.action);
      console.log('getCategory to ',nextProps);
      return this.getCategory(nextProps.categoryID);
    }else if(nextProps.categoryID || nextProps.action === 'delete'){
      return this.getCategory(nextProps.categoryID);
    }
  };//componentWillReceiveProps();

  checkStatusResponse(){
    const closeAlert = (e) => {
      if(e)
        e.preventDefault();
      this.setState({ status_response : null, message_response : null});
    };//closeAlert(e);
    if(this.state.status_response === true){
      setTimeout(() => {
        closeAlert();
      },4000);
      return (<Alert bsStyle={'success'} onDismiss={closeAlert.bind(this)}>{this.state.message_response}</Alert>);
    }

    if(this.state.status_response === false){
      setTimeout(() => {
        closeAlert();
      },4000);
      if(this.state.status === 'Rascunho'){
        return (<Alert className={'default'} onDismiss={closeAlert.bind(this)}>{this.state.message_response}</Alert>);
      }else if(this.state.status === 'requiredInput'){
        return (<Alert className={'warning'} onDismiss={closeAlert.bind(this)}>{this.state.message_response}</Alert>);
      }
      return (<Alert bsStyle={'danger'} onDismiss={closeAlert.bind(this)}>{this.state.message_response}</Alert>);
    }

    return null;
  };//checkStatusResponse();

  checkStates(){
    if(this.state.categoryID)
      this.setState({ categoryID : this.state.categoryID })

    console.log('checkStates exit();');
  };//checkStates();

  getCategory(categoryID){
    // console.log('Init getCategory ->',categoryID);
    if( categoryID ){
      // console.log('Getting categoryID ->',categoryID);
      // eslint-disable-next-line
      return this.state.categories.map((category) => {
        // eslint-disable-next-line
        if(category.categoryID === categoryID){
          this.setState({
            categoryID : category.categoryID,
            title : category.title,
            slug : category.slug,
            description : category.description,
            status: category.status
          });

          // eslint-disable-next-line
          return this.state.pages.map((page) => {
            // eslint-disable-next-line
            if(page.category === category.slug){
              console.log('page.category true',category.slug);
              this.setState({
                haveposts: true
              });
            }
          });
        }else{
          return false;
        }
      });
    }
    this.setState({ isLoading : false });
  };//getCategory();

  resetCurrentState(){
    return this.setState({
      showModal : false,
      confirmDelete : false,
      action : this.props.action,

      uid : this.props.currentUser.uid,
      categoryID : null,
      title : null,
      slug : null,
      description : '',
      status : true,
      haveposts : false,

      status_response : null,
      message_response : null
    });
  };//resetCurrentState();

  renderModal(){
    let
      categoryTitle = this.state.title;
    const close = () => {
      this.setState({ showModal : false });
    }
    const confirm = () => {
      firebase.database().ref('apps/'+ this.state.appID +'/categories/'+this.state.categoryID)
        .remove()
        .then(() => {
          this.setState({
            isLoading : false,
            showModal : false,
            confirmDelete : false,
            status : 'Deletado',
            status_response : true,
            message_response : '{ ' + this.state.title + ' } deletado com sucesso!'
          });
        })
        .catch((err) => {
          this.setState({
            isLoading : false,
            showModal : false,
            confirmDelete : true,
            status : 'Error',
            status_response : false,
            message_response : 'Ocorreu uma falha ao tentar deletar { ' + this.state.title + ' } - '+err.message
          });
        });
        console.clear();
    }
    return (
      (this.state.haveposts === true) ?
        <Modal show={this.state.showModal} onHide={close}>
          <Modal.Header closeButton>
            <Modal.Title>A categoria "{categoryTitle}" está sendo usada.</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <p>Há conteúdos associados à esta categoria. Por favor, verifique e tente novamente.</p>
          </Modal.Body>
          <Modal.Footer>
            <Link to={'/dashboard/categories'} onClick={close.bind(this)} className={'btn btn-warning'}>VOLTAR</Link>
          </Modal.Footer>
        </Modal>
      :
        <Modal show={this.state.showModal} onHide={close}>
          <Modal.Header closeButton>
            <Modal.Title>Deseja realmente excluir {categoryTitle}?</Modal.Title>
          </Modal.Header>
          <Modal.Footer>
            <Link to={'/dashboard/categories'} onClick={close.bind(this)} className={'btn btn-danger'}>Cancelar</Link>
            <Button onClick={confirm.bind(this)} bsStyle={'success'}>Confirmar</Button>
          </Modal.Footer>
        </Modal>
    );
  };//renderModal();

  /*** FUNCTIONS AUXILIARES DO FORM ***/
  onLoadForm(){
    console.log('onLoadForm();');
  };//onLoadForm(e);
  onSubmitForm(e) {
    e.preventDefault();
    let required = document.getElementsByClassName('required');
    if(required.length > 0){
      this.setState({
        isLoading : true,
        status : 'requiredInput',
        status_response : false,
        message_response : 'Por favor, preencha todos os campos corretamente.'
      });
      return false;
    }else{
      const categoryID = this.refs.categoryID.value;
      const dataInsert = {
        uid : this.state.uid,
        categoryID : categoryID,
        title : this.state.title,
        slug : this.state.slug.toLowerCase(),
        description : this.state.description,
        parent : this.state.parent,
        status: 'Publicado',
        createAt: TOOLS.getDate(),
        updateAt: TOOLS.getDate(),
      };
      firebase.database().ref('apps/'+ this.state.appID +'/categories/'+categoryID)
        .set(dataInsert, () => {
          this.setState({
            isLoading : false,
            status_response : true,
            message_response : '{ ' + dataInsert.title + ' } gravado com sucesso!'
          });
          console.clear();
          browserHistory.push('/dashboard/categories');
        });
    }
    return false;
  };//onSubmitForm(e);
  onUpdateForm(e){
    e.preventDefault();
    let required = document.getElementsByClassName('required');
    if(required.length > 0){
      this.setState({
        isLoading : true,
        status : 'requiredInput',
        status_response : false,
        message_response : 'Por favor, preencha todos os campos corretamente.'
      });
      return false;
    }else{
      const categoryID = this.state.categoryID;
      const dataUpdate = {
        uid : this.state.uid,
        categoryID : categoryID,
        title : this.state.title,
        slug : this.state.slug.toLowerCase(),
        description : this.state.description,
        parent : this.state.parent,
        status: this.state.status,
        createAt: this.state.createAt,
        updateAt: this.state.updateAt
      };
      console.log('dataUpdate -> ',dataUpdate);
      firebase.database().ref('apps/'+ this.state.appID +'/categories/'+categoryID)
        .update(dataUpdate, () => {
          this.setState({
            isLoading : false,
            uid : this.state.uid,
            categoryID : null,
            title : null,
            slug : null,
            description : null,
            parent : 'Página Master',
            status: null,
            createAt: this.state.createAt,
            updateAt: this.state.updateAt,

            status_response : true,
            message_response : '{ ' + this.state.title + ' } gravado com sucesso!'
          });
          console.clear();
          browserHistory.push('/dashboard/categories');
        });
    }
    return false;
  };//onUpdateForm(e);
  onChangeForm(e){
    console.log('onChangeForm',e)
    if(e){
      // eslint-disable-next-line
      // this.state.categories.map((category) => {
      //   if(category.slug === slug(e.target.value)){
      //     this.setState({
      //       categoryID : this.refs.categoryID.value,
      //       title : this.refs.title.value,
      //       slug: slug(this.refs.title.value)+'_2',
      //       // description : this.state.description,
      //       status: this.state.status
      //     });
      //   }else{
      //     this.setState({
      //       categoryID : this.refs.categoryID.value,
      //       title : this.refs.title.value,
      //       slug: slug(this.refs.title.value),
      //       // description : this.state.description,
      //       status: this.state.status
      //     });
      //   }
      // });
    }
  };//onChangeForm(e);
  _onChangeTitle(e){
    if(e){
      this.setState({
        title: e.target.value,
        slug: slug(e.target.value),
        updateAt: TOOLS.getDate(),
      });
    }
  };//onChangeInput(e);
  onChangeDescription(value){
    console.log(value);
    this.setState({ description : value });
  };//onChangeDescription(e);

  renderParent(){
    const renderDropdownCategories = () => {
      if(this.state.categories.length > 0){
        return this.state.categories.map((category) => {
          console.log('--> ',category)
          return (
            <MenuItem key={category.categoryID} className={'col-xs-12 col-md-12 no-padding'} onClick={this.onSelectCategory}>{category.title}</MenuItem>
          );
        });
      }
    }
    return (
      <div>
        <label>Parent</label>
        { (this.state.categories.length > 0) ?
          <DropdownButton id={'categoryList'} className={'col-xs-12 col-md-12'} title={'Categoria Master'}>
            <MenuItem key={0} className={'col-xs-12 col-md-12 no-padding'} onClick={this.onSelectCategory}>{'Categoria Master'}</MenuItem>
            {renderDropdownCategories()}
          </DropdownButton>
        :
          <Link to={'/dashboard/categories/new'} className={'categoryList btn-sm btn-warning text-center no-radius col-xs-12 col-md-12'} style={{marginTop:20}} title={'clique e cadastre uma categoria'}>Nenhuma categoria cadastrada</Link>
        }
      </div>
    );
  };//renderParent();
  onSelectCategory(e){
    if(e && e.target.innerHTML !== ''){
      e.preventDefault();
      this.setState({ parent : e.target.innerHTML });
      console.log(e.target.innerHTML);
    }
  };//onSelectCategory();

  statusInfo(){
    if( this.state.status ){
      if(this.state.status === 'Publicado')
        return (
          <label style={{color:'#000',textTransform:'uppercase',position:'absolute',top:4,right:0}} className={'label no-padding'}>
            <span style={{color:'#000',textTransform:'uppercase'}} className={'label'}>Status:</span> <span ref={'status'} className={ 'label label-success' }>{'Publicado'}</span>
          </label>
        );

      if(this.state.status === 'Rascunho')
        return (
          <label style={{color:'#000',textTransform:'uppercase',position:'absolute',top:4,right:0}} className={'label no-padding'}>
            <span style={{color:'#000',textTransform:'uppercase'}} className={'label'}>Status:</span> <span ref={'status'} className={ 'label label-default' }>{this.state.status || 'Aguardando publicação..'}</span>
          </label>
        );

      if(this.state.status === 'requiredInput')
        return (
          <label style={{color:'#000',textTransform:'uppercase',position:'absolute',top:4,right:0}} className={'label no-padding'}>
            <span style={{color:'#000',textTransform:'uppercase'}} className={'label'}>Status:</span> <span ref={'status'} className={ 'label label-default' }>{'Rascunho'}</span>
          </label>
        );
    }else{
      return (
        <label style={{color:'#000',textTransform:'uppercase',position:'absolute',top:4,right:0}} className={'label no-padding'}>
          <span style={{color:'#000',textTransform:'uppercase'}} className={'label'}>Status:</span> <span ref={'status'} className={ 'label label-default' }>{'Aguardando um título'}</span>
        </label>
      );
    }
  };//statusInfo();



  /*
   *	 RENDER FUNCTIONS - ACTIONS
   *
   *  Funções que renderizam a view do component,
   *  de acordo a ACTION de this.props.action;
   *  Todas as "ACTIONS Functions" definem document.title:
   *  Ex: document.title = APP_CONFIG.PROJECT_NAME + ' | Page Name';
   *  { actionNull - actionNew - actionEdit - actionDelete }
   */
  actionNull(){
    /*
     *  Responsável por renderizar a table de Categorias
     *  if( this.state.categories.length > 0 )
     */
    document.title = APP_CONFIG.PROJECT_NAME + ' | Conteúdo';
    if( !this.state.isLoading ){
      if( this.state.categories.length > 0 ){
        const renderList = () => {
          return this.state.categories.map((category) => {
            return (
              <tr key={category.categoryID}>
                <td style={{width:80}}>
                  <Link to={'/dashboard/categories/edit/'+category.categoryID} className={'btn btn-xs btn-primary'}>
                    <span className={'fa fa-pencil'}></span>
                  </Link>
                  <Link to={'/dashboard/categories/delete/'+category.categoryID} onClick={() => {this.setState({showModal:true,title:category.title,categoryID:category.categoryID})}} className={'btn btn-xs btn-danger'}>
                    <span className={'fa fa-trash'}></span>
                  </Link>
                </td>
                <td style={{width:60}}><small>{category.categoryID}</small></td>
                <td><small>{category.title}</small></td>
                <td><small>{TOOLS.stripAllTags(category.description).slice(0,49)+'..'}</small></td>
                <td><small>{category.slug}</small></td>
                <td><small>{category.createAt}</small></td>
                <td><small>{category.updateAt}</small></td>
                <td className={'text-center'}><small>{category.status}</small></td>
              </tr>
            );
          });
        }

        return (
          <div className={'table-responsive'}>
            <table className={'table table-hover'}>
              <thead>
                <tr>
                  <th></th>
                  <th><small>ID</small></th>
                  <th><small>TÍTULO</small></th>
                  <th><small>DESCRIÇÃO</small></th>
                  <th><small>SLUG</small></th>
                  <th><small>DATA DE INSERÇÃO</small></th>
                  <th><small>ÚLTIMA ALTERAÇÃO</small></th>
                  <th className={'text-center'}><small>STATUS</small></th>
                </tr>
              </thead>
              <tbody>
                { renderList() }
              </tbody>
            </table>
          </div>
        );
      }else{
        return (
          <Col xs={12} md={12} style={{textAlign:'center',padding:0}}>
            <div className="spinner">
              <div className="double-bounce1"></div>
              <div className="double-bounce2"></div>
            </div>
          </Col>
        );
      }
    }else{
      return (
        <div className={'table-responsive'}>
          <table className={'table table-hover'}>
            <thead>
              <tr>
                <th>ID</th>
                <th>TÍTULO</th>
                <th>DESCRIÇÃO</th>
                <th>SLUG</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td></td>
                <td className={'text-center'}>{'Nenhum registro foi encontrado... '}</td>
                <td></td>
                <td></td>
              </tr>
            </tbody>
          </table>
        </div>
      );
    }
  };//actionNull();

  actionNew(){
    /*
     *  Responsável por renderizar o Form para inserção de Categorias
     *  if( this.props.action === 'new' )
     */
    document.title = APP_CONFIG.PROJECT_NAME + ' | Adicionar Conteúdo';

    return (
      <div>
        <Col xs={12} md={12} className={'no-padding'}>
          <MasterForm onLoadForm={this.onLoadForm} onChange={this.onChangeForm} onSubmit={this.onSubmitForm}>
            <Col xs={12} md={8} className={'no-padding'}>
              <div className={'input-group col-md-12'}>
                <label>Título</label>
                <input
                  type={'hidden'}
                  ref={'categoryID'}
                  value={TOOLS.uniqueID()}
                  />
                  <input
                    type={'hidden'}
                    ref={'status'}
                    value={'Publicado'}
                    />
                <input
                  className={'form-control required'}
                  onBlur={TOOLS.requiredInput}
                  onChange={this._onChangeTitle}
                  type="text"
                  ref="title"
                  autoFocus
                  placeholder="Digite o título aqui" />
                  <label style={{color:'#000',textTransform:'uppercase',position:'absolute',top:4,right:0}} className={'label no-padding'}>
                    <span style={{color:'#000',textTransform:'uppercase'}} className={'label'}>Slug:</span> <span ref={'slug'} className={ this.state.slug ? 'label label-success' : 'label label-default'}>{this.state.slug ? this.state.slug : 'Aguardando um título..'}</span>
                  </label>
              </div>

              <div className={'input-group col-md-12'} style={{marginTop:20}}>
                <ReactQuill
                  theme={'snow'}
                  ref={'description'}
                  value={this.state.description}
                  onChange={this.onChangeDescription}
                  />
              </div>
            </Col>
            <Col xs={12} md={4} className={'no-paddingRight'}>
              {this.statusInfo()}
              {this.renderParent()}
              <Button
                onClick={this.onSubmitForm}
                className="btn btn-success pull-right no-radius"
                style={{marginTop:30}}
                >Publicar
              </Button>
              <Link
                to={'/dashboard/categories/'}
                className="btn btn-danger pull-right no-radius"
                style={{marginTop:30}}
                >Cancelar
              </Link>
            </Col>
          </MasterForm>
        </Col>
      </div>
    );
  };//actionNew();

  actionEdit(){
    /*
     *  Responsável por renderizar o Form para alteração de Categorias
     *  if( this.props.action === 'edit' )
     */
    document.title = APP_CONFIG.PROJECT_NAME + ' | Editar Conteúdo';

    if(this.props.action === 'edit'){
      console.log('action: ',this.props.action);
      if(this.props.categoryID){
        return (
          <div>
            <Col xs={12} md={12} className={'no-padding'}>
              <MasterForm onLoadForm={this.onLoadForm} onChange={this.onChangeForm} onSubmit={this.onUpdateForm}>
                <Col xs={12} md={8} className={'no-padding'}>
                  <div className={'input-group col-md-12'}>
                    <label>Título</label>
                    <FormControl
                      type={'hidden'}
                      ref={'categoryID'}
                      value={this.props.categoryID}
                      />
                    <FormControl
                      type={'hidden'}
                      ref={'status'}
                      value={'Publicado'}
                      />
                    <FormControl
                      className={'form-control'}
                      onBlur={TOOLS.requiredInput}
                      type={'text'}
                      ref={'title'}
                      onChange={this._onChangeTitle}
                      value={this.state.title || ''}
                      placeholder="Digite o título aqui" />
                      <label style={{color:'#000',textTransform:'uppercase',position:'absolute',top:4,right:0}} className={'label no-padding'}>
                        <span style={{color:'#000',textTransform:'uppercase'}} className={'label'}>Slug:</span> <span ref={'slug'} className={ this.state.slug ? 'label label-success' : 'label label-default'}>{this.state.slug ? this.state.slug : 'Aguardando um título..'}</span>
                      </label>
                  </div>

                  <div className={'input-group col-md-12'} style={{marginTop:20}}>
                    <ReactQuill
                      theme={'snow'}
                      ref={'description'}
                      value={this.state.description}
                      onChange={this.onChangeDescription}
                      />
                  </div>
                </Col>
                <Col xs={12} md={4} className={'no-paddingRight'}>
                  {this.statusInfo()}
                  {this.renderParent()}
                  <Button
                    onClick={this.onUpdateForm}
                    className="btn btn-success pull-right no-radius"
                    style={{marginTop:30}}
                    >Atualizar
                  </Button>
                  <Link
                    to={'/dashboard/categories/'}
                    className="btn btn-danger pull-right no-radius"
                    style={{marginTop:30}}
                    >Cancelar
                  </Link>
                </Col>
              </MasterForm>
            </Col>
          </div>
        );
      }else{
        return (<h1>Ops.. ocorreu uma falha..</h1>);
      }
      // console.log('categoryID: ',this.props.categoryID);
    }
  };//actionEdit();

  actionDelete(){
    /*
     *  Responsável por deletar categorias
     *  if( this.props.action === 'delete' )
     */
     if(this.props.action === 'delete'){
       if(this.props.categoryID){
         if(!this.state.confirmDelete){
           return (
             <div>
               {this.actionNull()}
               {this.renderModal()}
             </div>
           );
         }else{
           console.log('confirmDelete!');
           return (
             <div>
               {this.actionNull()}
             </div>
           );
         }
       }else{
         return (
           <h1>actionDelete #OFF</h1>
         );
       }
     }
  };//actionDelete();

  renderAction(){
    /*
     *  Responsável por verificar e chamar ACTIONS.
     *  Ex: if( this.props.action === 'new' ) this.actionNew();
     */
    if( this.props.action === 'new' )
      return this.actionNew();

    if(this.props.action === 'edit')
      return this.actionEdit();

    if(this.props.action === 'delete')
      return this.actionDelete();


    if( !this.props.action )
      return (<div>{this.actionNull()}</div>);
  };//renderAction();

  render(){
    return (
      <div>
        {this.checkStatusResponse()}
        {
          this.renderAction()
        }
      </div>
    );
  }
}
