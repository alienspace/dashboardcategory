import React, { Component } from 'react';
import {
  Col, Button,
  Alert,
  Modal, FormControl,
} from 'react-bootstrap';
import { browserHistory, Link, } from 'react-router';
import firebase from 'firebase';
import slug from 'slug';
import TinyMCE from 'react-tinymce';

/** COMPONENTS CUSTOM **/
import { APP_CONFIG } from './../../boot.config';
import { TOOLS } from './../../util/tools.global';
import { MasterForm } from '../Form/MasterForm';
export class Category extends Component{
  constructor(props){
    super(props);
    this.state = {
      isLoading : true,
      showModal : false,
      confirmDelete : false,
      action : this.props.action,
      categories : [],

      uid : this.props.currentUser.uid,
      categoryID : null,
      title : null,
      slug : null,
      description : null,
      status : false,

      status_response : null,
      message_response : null
    }

    this.checkStatusResponse = this.checkStatusResponse.bind(this);
    this.checkStates = this.checkStates.bind(this);
    this.getCategory = this.getCategory.bind(this);
    this.renderModal = this.renderModal.bind(this);
    this.requiredInput = TOOLS.requiredInput.bind(this);

    /*** FUNCTIONS AUXILIARES DO FORM ***/
    this.onLoadForm = this.onLoadForm.bind(this);
    this.onSubmitForm = this.onSubmitForm.bind(this);
    this.onUpdateForm = this.onUpdateForm.bind(this);
    this.onChangeForm = this.onChangeForm.bind(this);
    this._onChangeTitle = this._onChangeTitle.bind(this);
    this.onChangeTiny = this.onChangeTiny.bind(this);
    this.statusInfo = this.statusInfo.bind(this);

    /*** RENDER FUNCTIONS - ACTIONS ***/
    this.actionNull = this.actionNull.bind(this);
    this.actionNew = this.actionNew.bind(this);
    this.actionDelete = this.actionDelete.bind(this);
    this.renderAction = this.renderAction.bind(this);

    if(this.props.categoryID){
      this.getCategory(this.props.categoryID);
    }
  }

  componentWillMount(){
    // this.getCategory();
    if(this.props.action === 'edit' && this.props.categoryID){
      this.setState({ categoryID : this.props.categoryID });
    }
  }

  componentDidMount(){
    console.log('User id -> ',this.state.uid);
    let _self=this;
    _self.allCategories = firebase.database().ref('categories/');
    _self.categories = this.state.categories;

    /** child_added **/
    _self.allCategories.on('child_added', (data) => {
      _self.categories.push(data.val());
      if(data){
        if(_self.categories.length > 0 )
          _self.setState({ isLoading : false });

        _self.setState({ categories : _self.categories });
      }
      console.log('categories: ', {qtd: _self.categories.length, all: _self.categories});
    });
    /** child_added **/

    /** child_changed **/
    _self.allCategories.on('child_changed', (data) => {
      if(data){
        let changed = data.val();
        if(_self.categories.length > 0 ){
          console.log('child_changed categories: ', {changed : changed});
          // eslint-disable-next-line
          _self.categories.map((category) => {
            if(category.categoryID === changed.categoryID){
              category.uid = this.state.uid;
              category.title = changed.title;
              category.slug = changed.slug;
              category.description = changed.description;
              category.status = changed.status;
            }
          });
          _self.setState({ isLoading : false });
        }else{
          return false;
        }

        _self.setState({ categories : _self.categories });
      }
    });
    /** child_changed **/

    /** child_moved **/
    _self.allCategories.on('child_moved', (data) => {
      _self.allCategories.push(data.val());
      if(data){
        if(_self.allCategories.length > 0 )
          _self.setState({ isLoading : false });

        _self.setState({ allCategories : _self.allCategories });
      }
      console.log('child_moved allCategories: ', {qtd: _self.allCategories.length, all: _self.allCategories});
    });
    /** child_moved **/

    /** child_removed **/
    _self.allCategories.on('child_removed', (data) => {
      if(data){
        if(_self.categories.length > 0 ){
          _self.categories.pop(data.val());
          _self.setState({ isLoading : false });
        }

        _self.setState({ categories : _self.categories });
      }
      console.log('child_moved categories: ', {qtd: _self.categories.length, all: _self.categories});
    });
    /** child_removed **/
  };//componentDidMount();

  componentWillReceiveProps(nextProps){
    console.log('componentWillReceiveProps ',nextProps);

    if(nextProps.categoryID || nextProps.action === 'edit'){
      console.log('getCategory to ',nextProps.action);
      console.log('getCategory to ',nextProps);
      this.getCategory(nextProps.categoryID);
    }
  };//componentDidUpdate();

  componentDidUpdate(){

  }

  checkStatusResponse(){
    const closeAlert = (e) => {
      if(e)
        e.preventDefault();
      this.setState({ status_response : null, message_response : null});
    };//closeAlert(e);
    if(this.state.status_response === true){
      setTimeout(() => {
        closeAlert();
      },4000);
      return (<Alert bsStyle={'success'} onDismiss={closeAlert.bind(this)}>{this.state.message_response}</Alert>);
    }

    if(this.state.status_response === false){
      setTimeout(() => {
        closeAlert();
      },4000);
      if(this.state.status === 'Rascunho'){
        return (<Alert className={'default'} onDismiss={closeAlert.bind(this)}>{this.state.message_response}</Alert>);
      }else if(this.state.status === 'requiredInput'){
        return (<Alert className={'warning'} onDismiss={closeAlert.bind(this)}>{this.state.message_response}</Alert>);
      }
      return (<Alert bsStyle={'danger'} onDismiss={closeAlert.bind(this)}>{this.state.message_response}</Alert>);
    }

    return null;
  };//checkStatusResponse();

  checkStates(){
    if(this.state.categoryID)
      this.setState({ categoryID : this.state.categoryID })

    console.log('checkStates exit();');
  };//checkStates();

  getCategory(categoryID){
    // eslint-disable-next-line
    categoryID = parseInt(categoryID);
    if( categoryID ){
      console.log('Getting categoryID ->',categoryID);
      // eslint-disable-next-line
      return this.state.categories.map((category) => {
        // eslint-disable-next-line
        if(parseInt(category.categoryID) === categoryID){
          this.setState({
            categoryID : category.categoryID,
            title : category.title,
            slug : category.slug,
            description : category.description,
            status: category.status
          });
        }else{
          return false;
        }
      });
    }
    this.setState({ isLoading : false });
  };//getPages();

  renderModal(){
    let
      categoryTitle = this.state.title;
    const close = () => {
      this.setState({ showModal : false });
    }
    const confirm = () => {
      firebase.database().ref('categories/'+this.state.categoryID)
        .remove()
        .then(() => {
          this.setState({
            isLoading : false,
            showModal : false,
            confirmDelete : false,
            status : 'Deletado',
            status_response : true,
            message_response : '{ ' + this.state.title + ' } deletado com sucesso!'
          });
        })
        .catch((err) => {
          this.setState({
            isLoading : false,
            showModal : false,
            confirmDelete : true,
            status : 'Error',
            status_response : false,
            message_response : 'Ocorreu uma falha ao tentar deletar { ' + this.state.title + ' } - '+err.message
          });
        });
    }

    return (
      <Modal show={this.state.showModal} onHide={close}>
        <Modal.Header closeButton>
          <Modal.Title>Deseja realmente excluir {categoryTitle}?</Modal.Title>
        </Modal.Header>
        <Modal.Footer>
          <Link to={'/dashboard/category'} onClick={close.bind(this)} className={'btn btn-danger'}>Cancelar</Link>
          <Link to={'/dashboard/category'} onClick={confirm.bind(this)} bsStyle={'success'}>Confirmar</Link>
        </Modal.Footer>
      </Modal>
    );
  };//renderModal();

  requiredInput(e){
    if(!e.target.value || e.target.value === ''){
      e.target.classList.add('required');
    }else{
      e.target.classList.remove('required');
    }
    return false;
  };//requiredInput(e);

  /*** FUNCTIONS AUXILIARES DO FORM ***/
  onLoadForm(){
    console.log('onLoadForm();');
    if(this.props.action !== 'edit'){
      this.setState({
        uid : this.props.currentUser.uid,
        categoryID : this.props.categoryID || null,
        title : null,
        slug : null,
        description : null,
        status : false
      });
      console.log('set onLoadForm -> ',{
        uid : this.props.currentUser.uid,
        categoryID : this.props.categoryID || null,
        title : null,
        slug : null,
        description : null,
        status : false
      });
    }
  };//onLoadForm(e);
  onSubmitForm(e) {
    e.preventDefault();
    let required = document.getElementsByClassName('required');
    if(required.length > 0){
      this.setState({
        isLoading : true,
        status : 'requiredInput',
        status_response : false,
        message_response : 'Por favor, preencha todos os campos corretamente.'
      });
      return false;
    }else{
      const categoryID = this.refs.categoryID.value;
      const dataInsert = {
        uid : this.state.uid,
        categoryID : categoryID,
        title : this.state.title,
        slug : this.state.slug,
        description : this.state.description,
        status: this.state.status
      };
      firebase.database().ref('categories/'+categoryID)
        .set(dataInsert);
      this.setState({
        isLoading : false,
        status : 'Publicado',
        status_response : true,
        message_response : '{ ' + this.state.title + ' } gravado com sucesso!'
      });
      browserHistory.push('/dashboard/categories/');
    }
    return false;
  };//onSubmitForm(e);
  onUpdateForm(e){
    e.preventDefault();
    let required = document.getElementsByClassName('required');
    if(required.length > 0){
      this.setState({
        isLoading : true,
        status : 'requiredInput',
        status_response : false,
        message_response : 'Por favor, preencha todos os campos corretamente.'
      });
      return false;
    }else{
      const categoryID = this.props.categoryID;
      const dataUpdate = {
        uid : this.state.uid,
        categoryID : categoryID,
        title : this.state.title,
        slug : this.state.slug,
        description : this.state.description,
        status: this.state.status
      };
      firebase.database().ref('categories/'+categoryID)
        .update(dataUpdate, () => {
          this.setState({
            isLoading : false,
            categoryID : categoryID,
            status : 'Publicado',
            status_response : true,
            message_response : '{ ' + this.state.title + ' } gravado com sucesso!'
          });
          console.log('Update successful ',this.state.categories);
          browserHistory.push('/dashboard/categories');
        });
    }
    return false;
  };//onUpdateForm(e);
  onChangeForm(e){
    console.log('onChangeForm',e)
    if(e){
      this.state.categories.map((category) => {
        if(category.slug === slug(e.target.value)){
          this.setState({
            categoryID : this.refs.categoryID.value,
            title : this.refs.title.value,
            slug: slug(this.refs.title.value),
            description : this.refs.description.value,
            status: this.refs.status.value
          });
        }else{
          this.setState({
            categoryID : this.refs.categoryID.value,
            title : this.refs.title.value,
            slug: slug(this.refs.title.value),
            description : this.refs.description.value,
            status: this.refs.status.value
          });
        }
      });
    }

    console.log('-> ',{
      categoryID : this.refs.categoryID.value,
      title : this.refs.title.value,
      slug: slug(this.refs.title.value),
      description : this.state.description,
      status: this.refs.status.value
    })
  };//onChangeForm(e);
  _onChangeTitle(e){
    if(e){
      this.setState({title: e.target.value, slug: slug(e.target.value)});
    }
  };//onChangeInput(e);
  onChangeTiny(e){
    if(e)
      this.setState({ description : e.target.getContent() });
  };//onChangeTiny(e);

  statusInfo(){
    if( this.state.status_response === false ){
      if(this.state.status === 'Rascunho')
        return (
          <label style={{color:'#000',textTransform:'uppercase',position:'absolute',top:4,right:0}} className={'label no-padding'}>
            <span style={{color:'#000',textTransform:'uppercase'}} className={'label'}>Status:</span> <span ref={'slug'} className={ 'label label-default' }>{this.state.status || 'Aguardando publicação..'}</span>
          </label>
        );

      if(this.state.status === 'requiredInput')
        return (
          <label style={{color:'#000',textTransform:'uppercase',position:'absolute',top:4,right:0}} className={'label no-padding'}>
            <span style={{color:'#000',textTransform:'uppercase'}} className={'label'}>Status:</span> <span ref={'slug'} className={ 'label label-default' }>{'Rascunho'}</span>
          </label>
        );
    }else if(this.state.status_response === true){
      return (
        <label style={{color:'#000',textTransform:'uppercase',position:'absolute',top:4,right:0}} className={'label no-padding'}>
          <span style={{color:'#000',textTransform:'uppercase'}} className={'label'}>Status:</span> <span ref={'slug'} className={ 'label label-success' }>{'Rascunho'}</span>
        </label>
      );
    }else{
      return (
        <label style={{color:'#000',textTransform:'uppercase',position:'absolute',top:4,right:0}} className={'label no-padding'}>
          <span style={{color:'#000',textTransform:'uppercase'}} className={'label'}>Status:</span> <span ref={'slug'} className={ 'label label-default' }>{'Aguardando um título'}</span>
        </label>
      );
    }
  };//statusInfo();



  /*
   *	 RENDER FUNCTIONS - ACTIONS
   *
   *  Funções que renderizam a view do component,
   *  de acordo a ACTION de this.props.action;
   *  Todas as "ACTIONS Functions" definem document.title:
   *  Ex: document.title = APP_CONFIG.PROJECT_NAME + ' | Page Name';
   *  { actionNull - actionNew - actionEdit - actionDelete }
   */
  actionNull(){
    /*
     *  Responsável por renderizar a table de Categorias
     *  if( this.state.categories.length > 0 )
     */
    document.title = APP_CONFIG.PROJECT_NAME + ' | Categoria';
    if( !this.state.isLoading ){
      if( this.state.categories.length > 0 ){
        const renderList = () => {
          return this.state.categories.map((category) => {
            return (
              <tr key={category.categoryID}>
                <td>
                  <Link to={'/dashboard/categories/edit/'+category.categoryID} className={'btn btn-xs btn-primary'}>
                    <span className={'fa fa-pencil'}></span>
                  </Link>
                  <Link to={'/dashboard/categories/delete/'+category.categoryID} onClick={() => {this.setState({showModal:true,title:category.title,categoryID:category.categoryID})}} className={'btn btn-xs btn-danger'}>
                    <span className={'fa fa-trash'}></span>
                  </Link>
                </td>
                <td>{category.title}</td>
                <td>{category.description}</td>
                <td>{category.slug}</td>
              </tr>
            );
          });
        }

        return (
          <div className={'table-responsive'}>
            <table className={'table table-hover'}>
              <thead>
                <tr>
                  <th>ID</th>
                  <th>TÍTULO</th>
                  <th>DESCRIÇÃO</th>
                  <th>SLUG</th>
                </tr>
              </thead>
              <tbody>
                { renderList() }
              </tbody>
            </table>
          </div>
        );
      }else{
        return (
          <Col xs={12} md={12} style={{textAlign:'center',padding:0}}>
            <div className="spinner">
              <div className="double-bounce1"></div>
              <div className="double-bounce2"></div>
            </div>
          </Col>
        );
      }
    }else{
      return (
        <div className={'table-responsive'}>
          <table className={'table table-hover'}>
            <thead>
              <tr>
                <th>ID</th>
                <th>TÍTULO</th>
                <th>DESCRIÇÃO</th>
                <th>SLUG</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td></td>
                <td className={'text-center'}>{'Nenhum registro foi encontrado... '}</td>
                <td></td>
                <td></td>
              </tr>
            </tbody>
          </table>
        </div>
      );
    }
  };//actionNull();

  actionNew(){
    /*
     *  Responsável por renderizar o Form para inserção de Categorias
     *  if( this.props.action === 'new' )
     */
    document.title = APP_CONFIG.PROJECT_NAME + ' | Adicionar Categorias';

    return (
      <div>
        <Col xs={12} md={12} className={'no-padding'}>
          <form onChange={this.onChangeForm} onSubmit={this.onSubmitForm}>
            <Col xs={12} md={8} className={'no-padding'}>
              <div className={'input-group col-md-12'}>
                <label>Título</label>
                <input
                  type={'hidden'}
                  ref={'categoryID'}
                  value={TOOLS.uniqueID()}
                  />
                  <input
                    type={'hidden'}
                    ref={'status'}
                    value={'Publicado'}
                    />
                <input
                  className={'form-control required'}
                  onBlur={TOOLS.requiredInput}
                  onChange={this._onChangeTitle}
                  type="text"
                  ref="title"
                  placeholder="Digite o título aqui" />
                  <label style={{color:'#000',textTransform:'uppercase',position:'absolute',top:4,right:0}} className={'label no-padding'}>
                    <span style={{color:'#000',textTransform:'uppercase'}} className={'label'}>Slug:</span> <span ref={'slug'} className={ this.state.slug ? 'label label-success' : 'label label-default'}>{this.state.slug ? this.state.slug : 'Aguardando um título..'}</span>
                  </label>
              </div>

              <div className={'input-group col-md-12'} style={{marginTop:20}}>
                <TinyMCE
                  content={this.state.description}
                  ref={'description'}
                  config={{
                    skin: 'lightgray',
                    statusbar: false,
                    menubar: false,
                    height: 280,
                    plugins: 'link image code',
                    toolbar: 'undo redo | bold italic | alignleft aligncenter alignright | code'
                  }}
                  onChange={this.onChangeTiny}
                />
              </div>
            </Col>
            <Col xs={12} md={4} className={'no-paddingRight'}>
              {this.statusInfo}
              <Button
                onClick={this.onSubmitForm}
                className="btn btn-success pull-right no-radius"
                style={{marginTop:30}}
                >Publicar
              </Button>
              <Link
                to={'/dashboard/categoriess/'}
                className="btn btn-danger pull-right no-radius"
                style={{marginTop:30}}
                >Cancelar
              </Link>
            </Col>
          </form>
        </Col>
      </div>
    );
  };//actionNew();

  actionEdit(){
    /*
     *  Responsável por renderizar o Form para alteração de Categorias
     *  if( this.props.action === 'edit' )
     */
    document.title = APP_CONFIG.PROJECT_NAME + ' | Editar Categoria';

    if(this.props.action === 'edit'){
      console.log('action: ',this.props.action);
      if(this.props.categoryID){
        return (
          <div>
            <Col xs={12} md={12} className={'no-padding'}>
              <MasterForm onLoadForm={this.onLoadForm} onChange={this.onChangeForm} onSubmit={this.onUpdateForm}>
                <Col xs={12} md={8} className={'no-padding'}>
                  <div className={'input-group col-md-12'}>
                    <label>Título</label>
                    <FormControl
                      type={'hidden'}
                      ref={'categoryID'}
                      value={this.props.categoryID}
                      />
                      <input
                        type={'hidden'}
                        ref={'status'}
                        value={'Publicado'}
                        />
                    <FormControl
                      className={'form-control'}
                      onBlur={TOOLS.requiredInput}
                      type="text"
                      ref={'title'}
                      onChange={this._onChangeTitle}
                      value={this.state.title}
                      placeholder="Digite o título aqui" />
                      <label style={{color:'#000',textTransform:'uppercase',position:'absolute',top:4,right:0}} className={'label no-padding'}>
                        <span style={{color:'#000',textTransform:'uppercase'}} className={'label'}>Slug:</span> <span ref={'slug'} className={ this.state.slug ? 'label label-success' : 'label label-default'}>{this.state.slug ? this.state.slug : 'Aguardando um título..'}</span>
                      </label>
                  </div>

                  <div className={'input-group col-md-12'} style={{marginTop:20}}>
                    <TinyMCE
                      content={this.state.description ? this.state.description : 'ERROR'}
                      config={{
                        selector: "textarea",
                        skin: 'lightgray',
                        statusbar: false,
                        menubar: false,
                        height: 280,
                        plugins: 'link image code table emoticons textcolor colorpicker lists',
                        toolbar: 'undo redo | bold italic | alignleft aligncenter alignright | code'
                      }}
                      onChange={this.onChangeTiny}
                    />
                  </div>
                </Col>
                <Col xs={12} md={4} className={'no-paddingRight'}>
                  {this.statusInfo()}
                  <Button
                    onClick={this.onUpdateForm}
                    className="btn btn-success pull-right no-radius"
                    style={{marginTop:30}}
                    >Atualizar
                  </Button>
                  <Link
                    to={'/dashboard/categories/'}
                    className="btn btn-danger pull-right no-radius"
                    style={{marginTop:30}}
                    >Cancelar
                  </Link>
                </Col>
              </MasterForm>
            </Col>
          </div>
        );
      }else{
        return (<h1>Ops.. ocorreu uma falha..</h1>);
      }
      // console.log('categoryID: ',this.props.categoryID);
    }
  };//actionEdit();

  actionDelete(){
    /*
     *  Responsável por deletar categorias
     *  if( this.props.action === 'delete' )
     */
     if(this.props.action === 'delete'){
       if(this.props.categoryID){
         if(!this.state.confirmDelete){
           return (
             <div>
               {this.actionNull()}
               {this.renderModal()}
             </div>
           );
         }else{
           console.log('confirmDelete!');
           return (
             <div>
               {this.actionNull()}
             </div>
           );
         }
       }else{
         return (
           <h1>actionDelete #OFF</h1>
         );
       }
     }
  };//actionDelete();

  renderAction(){
    /*
     *  Responsável por verificar e chamar ACTIONS.
     *  Ex: if( this.props.action === 'new' ) this.actionNew();
     */
    if( this.props.action === 'new' )
      return this.actionNew();

    if(this.props.action === 'edit')
      return this.actionEdit();

    if(this.props.action === 'delete')
      return this.actionDelete();


    if( !this.props.action )
      return (<div>{this.actionNull()}</div>);
  };//renderAction();

  render(){
    return (
      <div>
        {this.checkStatusResponse()}
        {
          this.renderAction()
        }
      </div>
    );
  }
}
